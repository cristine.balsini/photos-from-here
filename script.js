function findLocation(func1) {
  const myMap = document.querySelector('#myMap');
  myMap.href = '';  //na URL já esta o safe_search=1 e per_page=5
  if (!navigator.geolocation) {
    status.textContent = 'Geolocation is not supported by your browser';
  } else {
    status.textContent = 'Locating…';
    navigator.geolocation.getCurrentPosition(func1); //mostra localização atual ou não (recebeu duas callback)
  }
}

function theLocation(position) {
  let inputText = document.getElementById('input').value;
  const latitude = position.coords.latitude;
  const longitude = position.coords.longitude;
  console.log(position)
  myMap.href = `https://shrouded-mountain-15003.herokuapp.com/https://flickr.com/services/rest/?api_key=bed33708122aa45c12752f21b0734255&format=json&nojsoncallback=1&method=flickr.photos.search&safe_search=1&per_page=5&lat=${latitude}&lon=${longitude}&text=${inputText}"`;
  myMap.textContent = ` Latitude: ${latitude} °, Longitude: ${longitude} °`;

  requisition(myMap.href)  //chama função que faz o fetch e armazena o objeto
}


function requisition(x) { //função que faz o fetch, armazena objeto e mostra imagem no browser (com callback)
  let inputTextDefault = document.getElementById('input').value;
  let latitudeDefault = -25.4295;  //default é Curitiba
  let longitudeDefault = -49.2712;

  fetch(x)
    .then(resposta => resposta.json())
    .then(data => {
      console.log('Dados da resposta:', data)

      if (data.photos.total === '0') { //caso região nao tenha fotos, redireciona
        textLocation.textContent = ` Latitude: ${latitudeDefault} °, Longitude: ${longitudeDefault} °`;
        console.log('Não há fotos na região atual. Redirecionando para Curitiba/PR.')
        fetch(`https://shrouded-mountain-15003.herokuapp.com/https://flickr.com/services/rest/?api_key=bed33708122aa45c12752f21b0734255&format=json&nojsoncallback=1&method=flickr.photos.search&safe_search=1&per_page=5&lat=${latitudeDefault}&lon=${longitudeDefault}&text=${inputTextDefault}"`)
          .then(resposta => resposta.json())
          .then(data => { browserImg(data); console.log('Dados da resposta Curitiba/PR:', data) })
      }
      else {  //mostra fotos da região do usuario caso exista
        browserImg(data)
      }
    });
}

function browserImg(y) {    //armazena URLs de imagens em array e printa na tela com função createElement
  let localImg = [];
  let localArray = [];

  for (let valor in y) {
    localArray.push(y[valor])
  }

  for (let i = 0; i < localArray[0].photo.length; i++) {   //armazena as URLs das imagens no array myArray
    localImg.push(`https://farm${localArray[0].photo[i].farm}.staticflickr.com/${localArray[0].photo[i].server}/${localArray[0].photo[i].id}_${localArray[0].photo[i].secret}.jpg`);
  }
  createElement(localImg[0], 1000);
  createElement(localImg[1], 3000);
  createElement(localImg[2], 5000);
  createElement(localImg[3], 7000);
  createElement(localImg[4], 9000);
}

function createElement(myPhoto, timer) {
  setTimeout(function () {
    let myElement = document.createElement('img');
    myElement.id = 'imageID'
    myElement.className = 'dinamicImg';
    myElement.src = myPhoto;
    myImage.appendChild(myElement);
  }, timer)
}

myButton.addEventListener('click', function () {
  document.body.style.backgroundColor = '#F00F07';
  asideBox.style.backgroundColor = '#404CFF';

  findLocation(theLocation);
  document.location.reload = true;
  myImage.innerHTML = '';
});


//Botão para busca em inglês (extra). Este botão não acessa o geoLocation.
myButton2.addEventListener('click', function () {
  document.body.style.backgroundColor = '#404CFF';
  asideBox.style.backgroundColor = '#F00F07';

  const latitude2 = 25.761681;
  const longitude2 = -80.191788;
  let inputText2 = document.getElementById('input2').value;
  myMap.href = `https://shrouded-mountain-15003.herokuapp.com/https://flickr.com/services/rest/?api_key=bed33708122aa45c12752f21b0734255&format=json&nojsoncallback=1&method=flickr.photos.search&safe_search=1&per_page=5&lat=${latitude2}&lon=${longitude2}&text=${inputText2}"`;

  fetch(myMap.href)
    .then(resposta => resposta.json())
    .then(data => {
      browserImg(data);
      console.log('Dados da resposta Miami, EUA', data)
      myMap.innerHTML = '';
      myMap.textContent = `Latitude: ${latitude2} °  Longitude: ${longitude2} °  - Miami (EUA)`;
    })
  document.location.reload = true;
  myImage.innerHTML = '';
})


